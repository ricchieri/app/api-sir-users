FROM openjdk:15-jdk-alpine

ARG ENVIRONMENT

COPY api-sir-users-service/conf/${ENVIRONMENT} application/configuration/

COPY api-sir-users-service/target/api-sir-users-service-*.jar application/

RUN wget -O application/apm-agent.jar https://search.maven.org/remotecontent?filepath=co/elastic/apm/elastic-apm-agent/1.8.0/elastic-apm-agent-1.8.0.jar

CMD java -jar application/api-sir-users-service-*.jar --spring.config.location=application/configuration/

EXPOSE 8080