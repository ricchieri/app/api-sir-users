package com.ricchieri.api.sir.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CompanyDTO {

    @NotEmpty(message = "COMPANY_NAME_EMPTY")
    private String name;

    @NotNull(message = "CONTACT_INFORMATION_EMPTY")
    private ContactInformationDTO contactInformation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
