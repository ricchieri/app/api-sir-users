package com.ricchieri.api.sir.users.request;

import java.io.Serializable;

import com.ricchieri.api.sir.users.enums.SearchOperation;
import com.ricchieri.api.sir.users.enums.SearchOperator;

public class SearchCriteria implements Serializable {

    private static final long serialVersionUID = 6172283512518939868L;

    private String key;
    private SearchOperation operation;
    private Object value;
    private SearchOperator operator;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public SearchOperation getOperation() {
        return operation;
    }

    public void setOperation(SearchOperation operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public SearchOperator getOperator() {
        return operator;
    }

    public void setOperator(SearchOperator operator) {
        this.operator = operator;
    }
}
