package com.ricchieri.api.sir.dto;

public abstract class CommentDTO {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}