package com.ricchieri.api.sir.users.enums;

import java.util.Optional;

public enum SearchOperation {
    NEGATION("!"), CONTAINS("%%"), STARTS_WITH("x%"), ENDS_WITH("%x"), EQUALS(":"), GREATER_THAN(
            ">"), GREATER_OR_EQUAL_THAN(">="), LESS_THAN("<"), LESS_OR_EQUAL_THAN("=<");

    private String code;

    SearchOperation(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static Optional<SearchOperation> findByCode(String code) {
        Optional<SearchOperation> operation = Optional.empty();
        for (SearchOperation searchOperation : values()) {
            if (searchOperation.getCode().equals(code)) {
                operation = Optional.of(searchOperation);
            }
        }
        return operation;
    }

    public static Optional<SearchOperation> findByName(String name) {
        Optional<SearchOperation> operation = Optional.empty();
        for (SearchOperation searchOperation : values()) {
            if (searchOperation.name().equals(name)) {
                operation = Optional.of(searchOperation);
            }
        }
        return operation;
    }
}
