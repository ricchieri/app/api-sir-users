package com.ricchieri.api.sir.users.request;

import java.util.List;
import org.springframework.data.domain.Pageable;

public class SearchRequest {

    private List<SearchCriteria> criterias;
    private Pageable pageable;

    public List<SearchCriteria> getCriterias() {
        return criterias;
    }

    public void setCriterias(List<SearchCriteria> criterias) {
        this.criterias = criterias;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
