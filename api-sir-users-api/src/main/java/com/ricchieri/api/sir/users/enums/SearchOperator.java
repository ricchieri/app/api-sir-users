package com.ricchieri.api.sir.users.enums;

import java.util.Optional;

public enum SearchOperator {
    AND("&&"), OR("||");

    private String code;

    SearchOperator(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static Optional<SearchOperator> findByCode(String code) {
        Optional<SearchOperator> operation = Optional.empty();
        for (SearchOperator searchOperation : values()) {
            if (searchOperation.getCode().equals(code)) {
                operation = Optional.of(searchOperation);
            }
        }
        return operation;
    }

    public static Optional<SearchOperator> findByName(String name) {
        Optional<SearchOperator> operation = Optional.empty();
        for (SearchOperator searchOperation : values()) {
            if (searchOperation.name().equals(name)) {
                operation = Optional.of(searchOperation);
            }
        }
        return operation;
    }
}
