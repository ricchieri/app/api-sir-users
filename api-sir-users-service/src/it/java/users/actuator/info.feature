Feature: Health

  Background:
    * url AppUrl
    
   Scenario: Info OK
  	Given path '/info'
    When method GET
    
    Then status 200
    Then match response == {build:{groupId:'com.ricchieri', artifactId:'api-sir-users-service', version: #notnull, appName: 'api-sir-users'}},
    
    
