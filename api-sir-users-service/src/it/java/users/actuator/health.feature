Feature: get health

  Background:
    * url AppUrl

    Scenario:
      Given path '/health'
      When method GET

      Then status 200
      Then match response.status == "UP"
