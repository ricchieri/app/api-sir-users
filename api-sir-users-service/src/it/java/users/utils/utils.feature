Feature: Utils

  Scenario:
    * def getDate =
      """
        function(daysToAdd, format) {
          var SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
          var Calendar = Java.type('java.util.Calendar');
          var sdf = new SimpleDateFormat(format == null ? 'yyyy-MM-dd' : format);

          var rightNow = Calendar.getInstance();

          if (daysToAdd != null) {
            rightNow.add(Calendar.DATE, daysToAdd);
          }

          return sdf.format(rightNow.getTime());
        }
        """