package com.ricchieri.api.sir.users.architecture;

public class ArchitectureConstants {

    public static final String MAPPER_PACKAGE = "..mapper..";
    public static final String CONTROLLER_PACKAGE = "..controller";
    public static final String RESOURCE_PACKAGE = "..controller.resource";
    public static final String SERVICE_PACKAGE = "..service..";
    public static final String CONNECTOR_PACKAGE = "..connector";
    public static final String CONNECTOR_CONFIGURATION_PACKAGE = "..connector.configuration";
    public static final String REPOSITORY_PACKAGE = "..repository";
    public static final String ENUMS_PACKAGE = "..enums";
    public static final String MODEL_PACKAGE = "..model";
    public static final String RESPONSE_PACKAGE = "..response";
    public static final String HELPER_PACKAGE = "..helper";

    public static final String DEFAULT_PACKAGE = "com.ricchieri.api.sir.users";

    public static final String MAPPER_PREFIX = "Mapper";
    public static final String CONTROLLER_PREFIX = "Controller";
    public static final String RESOURCE_PREFIX = "Resource";
    public static final String SERVICE_PREFIX = "Service";
    public static final String CONNECTOR_PREFIX = "Connector";
    public static final String CONNECTOR_CONFIGURATION_PREFIX = "ConnectorConfiguration";
    public static final String REPOSITORY_PREFIX = "Repository";

    private ArchitectureConstants() {

    }

}
