// package com.ricchieri.api.sir.users.architecture;
//
// import com.tngtech.archunit.core.importer.ImportOption;
// import com.tngtech.archunit.junit.AnalyzeClasses;
// import com.tngtech.archunit.junit.ArchTest;
// import com.tngtech.archunit.lang.ArchRule;
//
// import static com.tngtech.archunit.library.Architectures.layeredArchitecture;
//
// @AnalyzeClasses(packages = ArchitectureConstants.DEFAULT_PACKAGE, importOptions =
// ImportOption.DoNotIncludeTests.class)
// public class LayeredArchitectureTest {
//
// private static final String CONNECTORS = "Connectors";
// private static final String MAPPERS = "Mappers";
// private static final String REPOSITORIES = "Repositories";
// private static final String SERVICES = "Services";
// private static final String CONTROLLERS = "Controllers";
// private static final String CONNECTOR_CONFIGURATIONS = "ConnectorConfigurations";
// private static final String HELPERS = "Helpers";
// private static final String MODELS = "Models";
// private static final String RESPONSE = "Response";
//
// @ArchTest
// public static final ArchRule layerDependenciesAreRespected = layeredArchitecture()
// .layer(CONTROLLERS).definedBy(ArchitectureConstants.CONTROLLER_PACKAGE)
// .layer(SERVICES).definedBy(ArchitectureConstants.SERVICE_PACKAGE)
// .layer(MAPPERS).definedBy(ArchitectureConstants.MAPPER_PACKAGE)
// .layer(REPOSITORIES).definedBy(ArchitectureConstants.REPOSITORY_PACKAGE)
// .layer(CONNECTORS).definedBy(ArchitectureConstants.CONNECTOR_PACKAGE)
// .layer(CONNECTOR_CONFIGURATIONS).definedBy(ArchitectureConstants.CONNECTOR_CONFIGURATION_PACKAGE)
// .layer(HELPERS).definedBy(ArchitectureConstants.HELPER_PACKAGE)
// .layer(MODELS).definedBy(ArchitectureConstants.MODEL_PACKAGE)
// .layer(RESPONSE).definedBy(ArchitectureConstants.RESPONSE_PACKAGE)
//
// .whereLayer(CONTROLLERS).mayNotBeAccessedByAnyLayer()
// .whereLayer(SERVICES).mayOnlyBeAccessedByLayers(CONTROLLERS, SERVICES, HELPERS)
// .whereLayer(REPOSITORIES).mayOnlyBeAccessedByLayers(SERVICES)
// .whereLayer(MAPPERS).mayOnlyBeAccessedByLayers(SERVICES)
// .whereLayer(CONNECTORS).mayOnlyBeAccessedByLayers(SERVICES, CONNECTOR_CONFIGURATIONS)
// .whereLayer(HELPERS).mayOnlyBeAccessedByLayers(MAPPERS, SERVICES, REPOSITORIES)
// .whereLayer(MODELS)
// .mayOnlyBeAccessedByLayers(MAPPERS, SERVICES, REPOSITORIES, HELPERS, RESPONSE, CONNECTORS);
//
// }