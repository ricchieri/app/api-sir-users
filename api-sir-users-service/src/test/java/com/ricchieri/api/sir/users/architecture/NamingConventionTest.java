package com.ricchieri.api.sir.users.architecture;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@AnalyzeClasses(packages = ArchitectureConstants.DEFAULT_PACKAGE, importOptions = ImportOption.DoNotIncludeTests.class)
public class NamingConventionTest {

    // Name
    @ArchTest
    public static final ArchRule repositoryShouldBePrefixed = classes().that()
            .resideInAPackage(ArchitectureConstants.REPOSITORY_PACKAGE).and().areAnnotatedWith(Repository.class)
            .should().haveSimpleNameEndingWith(ArchitectureConstants.REPOSITORY_PREFIX).orShould()
            .haveSimpleNameEndingWith("RepositoryImpl");

    @ArchTest
    public static final ArchRule connectorConfigurationShouldBePrefixed = classes().that()
            .resideInAPackage(ArchitectureConstants.CONNECTOR_CONFIGURATION_PACKAGE).and()
            .areAnnotatedWith(Configuration.class).and().areAnnotatedWith(ConfigurationProperties.class).should()
            .haveSimpleNameEndingWith(ArchitectureConstants.CONNECTOR_CONFIGURATION_PREFIX);

    @ArchTest
    public static final ArchRule connectorShouldBePrefixed = classes().that().resideInAPackage("connector").should()
            .haveSimpleNameEndingWith(ArchitectureConstants.CONNECTOR_PREFIX);

    @ArchTest
    public static final ArchRule mapperShouldBePrefixed = classes().that().resideInAPackage("..service..(*)..mapper")
            .and().areAnnotatedWith(Component.class).should()
            .haveSimpleNameEndingWith(ArchitectureConstants.MAPPER_PREFIX);

    @ArchTest
    public static final ArchRule servicesShouldBePrefixed = classes().that()
            .resideInAPackage(ArchitectureConstants.SERVICE_PACKAGE).and().areAnnotatedWith(Service.class).should()
            .haveSimpleNameEndingWith(ArchitectureConstants.SERVICE_PREFIX);

    @ArchTest
    public static final ArchRule controllersShouldBePrefixed = classes().that()
            .resideInAPackage(ArchitectureConstants.CONTROLLER_PACKAGE).or().areAnnotatedWith(Controller.class).should()
            .haveSimpleNameEndingWith(ArchitectureConstants.CONTROLLER_PREFIX);

    // Reside
    @ArchTest
    public static final ArchRule enumsShouldBeInEnumsPackage = classes().that().areEnums().should()
            .resideInAPackage(ArchitectureConstants.ENUMS_PACKAGE);

    @ArchTest
    public static final ArchRule classesNamedRepositoryShouldBeInRepositoryPackage = classes().that()
            .haveSimpleNameContaining(ArchitectureConstants.REPOSITORY_PREFIX).should()
            .resideInAPackage(ArchitectureConstants.REPOSITORY_PACKAGE);

    @ArchTest
    public static final ArchRule classesNamedConnectorShouldBeInConnectorPackage = classes().that()
            .haveSimpleNameContaining(ArchitectureConstants.CONNECTOR_PREFIX).and()
            .resideOutsideOfPackage(ArchitectureConstants.CONNECTOR_CONFIGURATION_PACKAGE).should()
            .resideInAPackage(ArchitectureConstants.CONNECTOR_PACKAGE);

    @ArchTest
    public static final ArchRule classesNamedConnectorConfigurationShouldBeInServicePackage = classes().that()
            .haveSimpleNameContaining(ArchitectureConstants.CONNECTOR_CONFIGURATION_PREFIX).should()
            .resideInAPackage(ArchitectureConstants.CONNECTOR_CONFIGURATION_PACKAGE);

    @ArchTest
    public static final ArchRule classesNamedMapperShouldBeInMapperPackage = classes().that()
            .haveSimpleNameContaining(ArchitectureConstants.MAPPER_PREFIX).and()
            .resideOutsideOfPackage(ArchitectureConstants.DEFAULT_PACKAGE.concat(".mapper")).should()
            .resideInAPackage(ArchitectureConstants.MAPPER_PACKAGE);

    @ArchTest
    public static final ArchRule classesNamedServiceShouldBeInServicePackage = classes().that()
            .haveSimpleNameContaining(ArchitectureConstants.SERVICE_PREFIX).should()
            .resideInAPackage(ArchitectureConstants.SERVICE_PACKAGE);

    @ArchTest
    public static final ArchRule classesNamedControllerShouldBeInControllerPackage = classes().that()
            .haveSimpleNameContaining(ArchitectureConstants.CONTROLLER_PREFIX).should()
            .resideInAPackage(ArchitectureConstants.CONTROLLER_PACKAGE);

}