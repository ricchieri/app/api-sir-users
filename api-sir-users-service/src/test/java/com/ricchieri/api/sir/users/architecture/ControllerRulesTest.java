package com.ricchieri.api.sir.users.architecture;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.fields;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;

@AnalyzeClasses(packages = ArchitectureConstants.DEFAULT_PACKAGE, importOptions = ImportOption.DoNotIncludeTests.class)
public class ControllerRulesTest {

    @ArchTest
    private static final ArchRule methodsNotStatic = methods().that().arePublic().or().arePrivate().or().areProtected()
            .and().areDeclaredInClassesThat().resideInAPackage(ArchitectureConstants.CONTROLLER_PACKAGE).should()
            .notBeStatic();

    @ArchTest
    private static final ArchRule fieldsByFinal = fields().that().arePrivate().and().areDeclaredInClassesThat()
            .resideInAPackage(ArchitectureConstants.CONTROLLER_PACKAGE).should().beFinal();

    @ArchTest
    private static final ArchRule methodsReturnResponseEntity = methods().that().arePublic().and()
            .areDeclaredInClassesThat().resideInAPackage(ArchitectureConstants.CONTROLLER_PACKAGE).and()
            .areDeclaredInClassesThat().haveSimpleNameEndingWith(ArchitectureConstants.CONTROLLER_PREFIX).and()
            .areDeclaredInClassesThat().areAnnotatedWith(Controller.class).or().areDeclaredInClassesThat()
            .areAnnotatedWith(RestController.class).should().haveRawReturnType(ResponseEntity.class).orShould()
            .haveRawReturnType("void");

}
