package com.ricchieri.api.sir.users.architecture;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.fields;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

@AnalyzeClasses(packages = ArchitectureConstants.DEFAULT_PACKAGE, importOptions = ImportOption.DoNotIncludeTests.class)
public class ModelRulesTest {

    @ArchTest
    private static final ArchRule methodsInTheModelBePublic = methods().that().areDeclaredInClassesThat()
            .resideInAPackage(ArchitectureConstants.MODEL_PACKAGE).should().bePublic();

    @ArchTest
    private static final ArchRule methodsInTheModelNotStatic = methods().that().areDeclaredInClassesThat()
            .resideInAPackage(ArchitectureConstants.MODEL_PACKAGE).should().notBeStatic();

    @ArchTest
    private static final ArchRule fieldsInTheModelNotBeFinal = fields().that().areDeclaredInClassesThat()
            .resideInAPackage(ArchitectureConstants.MODEL_PACKAGE).should().notBeFinal();

    @ArchTest
    private static final ArchRule fieldsInTheModelNotBePublic = fields().that().areDeclaredInClassesThat()
            .resideInAPackage(ArchitectureConstants.MODEL_PACKAGE).should().notBePublic();

}
