CREATE TABLE company (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6),  
  name VARCHAR(100) NOT NULL, 
  creation_user_id BIGINT, 
  updated_user_id BIGINT, 
  contact_information_id BIGINT, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE company_comments (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  message TEXT NOT NULL, 
  creation_user_id BIGINT, 
  updated_user_id BIGINT, 
  company_id BIGINT, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE company_document (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  document_number VARCHAR(15) NOT NULL, 
  creation_user_id BIGINT, 
  updated_user_id BIGINT, 
  document_type_id BIGINT NOT NULL, 
  company_id BIGINT NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE company_users (
  company_id BIGINT NOT NULL, users_id BIGINT NOT NULL
) engine = InnoDB;

CREATE TABLE contact_email (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  email VARCHAR(80) NOT NULL, 
  is_default BIT, 
  email_type_id BIGINT NOT NULL, 
  user_id BIGINT NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE contact_information (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  creation_user_id BIGINT, 
  updated_user_id BIGINT, 
  location_id BIGINT, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE contact_information_emails (
  contact_information_id BIGINT NOT NULL, 
  emails_id BIGINT NOT NULL
) engine = InnoDB;

CREATE TABLE contact_information_phones (
  contact_information_id BIGINT NOT NULL, 
  phones_id BIGINT NOT NULL
) engine = InnoDB;

CREATE TABLE contact_location (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  city VARCHAR(250) NOT NULL, 
  floor VARCHAR(50), 
  street VARCHAR(250) NOT NULL, 
  suite VARCHAR(50), 
  zipcode VARCHAR(50), 
  creation_user_id BIGINT, 
  updated_user_id BIGINT, 
  state_id BIGINT, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE contact_phone (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  is_default BIT, 
  number VARCHAR(30) NOT NULL, 
  phone_type_id BIGINT NOT NULL, 
  user_id BIGINT NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE document_type (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  description VARCHAR(50) NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE email_type (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  description VARCHAR(50) NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE hibernate_sequence (next_val BIGINT) engine = InnoDB;

INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);
  
INSERT into hibernate_sequence 
VALUES 
  (1);

CREATE TABLE permission (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  description VARCHAR(50) NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE phone_type (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  description VARCHAR(50) NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE role (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  description VARCHAR(50) NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE role_permissions (
  role_id BIGINT NOT NULL, permissions_id BIGINT NOT NULL
) engine = InnoDB;

CREATE TABLE state (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  name VARCHAR(100) NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE user (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  birthday DATETIME(6), 
  first_name VARCHAR(100) NOT NULL, 
  last_login DATETIME(6), 
  last_name VARCHAR(100) NOT NULL, 
  login_attemp INTEGER NOT NULL, 
  password VARCHAR(256) NOT NULL, 
  username VARCHAR(11) NOT NULL, 
  contact_information_id BIGINT, 
  user_id BIGINT NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE user_comments (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  message TEXT NOT NULL, 
  creation_user_id BIGINT, 
  updated_user_id BIGINT, 
  user_id BIGINT, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE user_document (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  document_number VARCHAR(15) NOT NULL, 
  creation_user_id BIGINT, 
  updated_user_id BIGINT, 
  document_type_id BIGINT NOT NULL, 
  user_id BIGINT NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE user_extra_information (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  block BIT NOT NULL, 
  block_motive TEXT, 
  cancellation_charge BIT NOT NULL, 
  current_account BIT NOT NULL, 
  office_id BIGINT NOT NULL, 
  user_id BIGINT NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

CREATE TABLE user_role (
  id BIGINT NOT NULL, 
  active BIT NOT NULL, 
  optlock INTEGER NOT NULL, 
  creation_date DATETIME(6) NOT NULL, 
  updated_date DATETIME(6), 
  finish_date DATETIME(6), 
  start_date DATETIME(6) NOT NULL, 
  role_id BIGINT NOT NULL, 
  user_id BIGINT NOT NULL, 
  PRIMARY key (id)
) engine = InnoDB;

ALTER TABLE 
  contact_information_emails 
ADD 
  CONSTRAINT UK_pr5geye0qv9cyvh42047bx9ko UNIQUE (emails_id);
  
ALTER TABLE 
  contact_information_phones 
ADD 
  CONSTRAINT UK_5eaxem7yase23v1y16wfs9nr3 UNIQUE (phones_id);
  
ALTER TABLE 
  company 
ADD 
  CONSTRAINT FKr5hqe1kwbtxuohhr18mrjcd1k FOREIGN key (creation_user_id) REFERENCES user (id);
  
ALTER TABLE 
  company 
ADD 
  CONSTRAINT FKikrp79mbkarssaue5npfn2wip FOREIGN key (updated_user_id) REFERENCES user (id);
  
ALTER TABLE 
  company 
ADD 
  CONSTRAINT FK89tmoa5rn7taetq12ghbx7dhe FOREIGN key (contact_information_id) REFERENCES contact_information (id);
  
ALTER TABLE 
  company_comments 
ADD 
  CONSTRAINT FKdeme3r2qe8spexuxe2g4dpcgt FOREIGN key (creation_user_id) REFERENCES user (id);
  
ALTER TABLE 
  company_comments 
ADD 
  CONSTRAINT FK6c56vrxdj418ipnc226no6u6e FOREIGN key (updated_user_id) REFERENCES user (id);
  
ALTER TABLE 
  company_comments 
ADD 
  CONSTRAINT FKjcg3j9sdslecp2sb8w1rprlsc FOREIGN key (company_id) REFERENCES company (id);
  
ALTER TABLE 
  company_document 
ADD 
  CONSTRAINT FKfiux6vhs1hkq2jsb7qpq6vgut FOREIGN key (creation_user_id) REFERENCES user (id);
  
ALTER TABLE 
  company_document 
ADD 
  CONSTRAINT FKckqcylj6ut8roilcvau41pcat FOREIGN key (updated_user_id) REFERENCES user (id);
  
ALTER TABLE 
  company_document 
ADD 
  CONSTRAINT FK50j8ycysqhocliordc37jqkx1 FOREIGN key (document_type_id) REFERENCES document_type (id);
  
ALTER TABLE 
  company_document 
ADD 
  CONSTRAINT FKpvmv0x7y5x58w7igib3owramk FOREIGN key (company_id) REFERENCES company (id);
  
ALTER TABLE 
  company_users 
ADD 
  CONSTRAINT FKd6f6rmuup769kyrt22kbfcv6g FOREIGN key (users_id) REFERENCES user (id);
  
ALTER TABLE 
  company_users 
ADD 
  CONSTRAINT FKkwk4foy88lulxos847o0qt4ig FOREIGN key (company_id) REFERENCES company (id);
  
ALTER TABLE 
  contact_email 
ADD 
  CONSTRAINT FK7rtc6ju25loc9icpt9gg6ghgv FOREIGN key (email_type_id) REFERENCES email_type (id);
  
ALTER TABLE 
  contact_email 
ADD 
  CONSTRAINT FKs4821r154kf2pje8rokopjguy FOREIGN key (user_id) REFERENCES user (id);
  
ALTER TABLE 
  contact_information 
ADD 
  CONSTRAINT FKg8nba3s67k7uitdnj208xabpv FOREIGN key (creation_user_id) REFERENCES user (id);
  
ALTER TABLE 
  contact_information 
ADD 
  CONSTRAINT FKp31nr8dquvn9a9xvrcujxdf8e FOREIGN key (updated_user_id) REFERENCES user (id);
  
ALTER TABLE 
  contact_information 
ADD 
  CONSTRAINT FK4c04071akm548x4lqak1maw9c FOREIGN key (location_id) REFERENCES contact_location (id);
  
ALTER TABLE 
  contact_information_emails 
ADD 
  CONSTRAINT FKq8kah9udbyl25j4i7mi9kiymy FOREIGN key (emails_id) REFERENCES contact_email (id);
  
ALTER TABLE 
  contact_information_emails 
ADD 
  CONSTRAINT FKsl5u5l01aghhbod9ipi9cbclx FOREIGN key (contact_information_id) REFERENCES contact_information (id);
  
ALTER TABLE 
  contact_information_phones 
ADD 
  CONSTRAINT FKifdy29rukfb2ywnl7ld7cltqy FOREIGN key (phones_id) REFERENCES contact_phone (id);
  
ALTER TABLE 
  contact_information_phones 
ADD 
  CONSTRAINT FKs77jltg5sdcdwndon7334nh67 FOREIGN key (contact_information_id) REFERENCES contact_information (id);
  
ALTER TABLE 
  contact_location 
ADD 
  CONSTRAINT FKf6d2q4wt8m9wge0almixnn3eh FOREIGN key (creation_user_id) REFERENCES user (id);
  
ALTER TABLE 
  contact_location 
ADD 
  CONSTRAINT FKs2aouo0kwdv81vi8vk56177ln FOREIGN key (updated_user_id) REFERENCES user (id);
  
ALTER TABLE 
  contact_location 
ADD 
  CONSTRAINT FKbs6by1iucwgk5twngelxjwhay FOREIGN key (state_id) REFERENCES state (id);
  
ALTER TABLE 
  contact_phone 
ADD 
  CONSTRAINT FKmxgx5idyv26895i7jj1ov288c FOREIGN key (phone_type_id) REFERENCES phone_type (id);
  
ALTER TABLE 
  contact_phone 
ADD 
  CONSTRAINT FK4pe82deh4dw9rbc248ni145kl FOREIGN key (user_id) REFERENCES user (id);
  
ALTER TABLE 
  role_permissions 
ADD 
  CONSTRAINT FKclluu29apreb6osx6ogt4qe16 FOREIGN key (permissions_id) REFERENCES permission (id);
  
ALTER TABLE 
  role_permissions 
ADD 
  CONSTRAINT FKlodb7xh4a2xjv39gc3lsop95n FOREIGN key (role_id) REFERENCES role (id);
  
ALTER TABLE 
  user 
ADD 
  CONSTRAINT FKd6n162q6610mf20yxqyk8i770 FOREIGN key (contact_information_id) REFERENCES contact_information (id);
  
ALTER TABLE 
  user 
ADD 
  CONSTRAINT FKb93xx0vd6uth2mr99t6t4eolj FOREIGN key (user_id) REFERENCES user_extra_information (id);
  
ALTER TABLE 
  user_extra_information 
ADD 
  CONSTRAINT FKb93xx0vd6uth2mr90t6t4eolj FOREIGN key (user_id) REFERENCES user (id);  
  
ALTER TABLE 
  user_comments 
ADD 
  CONSTRAINT FKnb4fdjddfawmr4pv95j1om7g FOREIGN key (creation_user_id) REFERENCES user (id);
  
ALTER TABLE 
  user_comments 
ADD 
  CONSTRAINT FKai14fo248rg40q1rjv2hy890i FOREIGN key (updated_user_id) REFERENCES user (id);
  
ALTER TABLE 
  user_comments 
ADD 
  CONSTRAINT FKornrskknlmumgdhlohpbcvrw5 FOREIGN key (user_id) REFERENCES user (id);
  
ALTER TABLE 
  user_document 
ADD 
  CONSTRAINT FKbil5h6mmhj3hxrjylocd08m96 FOREIGN key (creation_user_id) REFERENCES user (id);
  
ALTER TABLE 
  user_document 
ADD 
  CONSTRAINT FKm13cl9bj1mt4givktpbjg9kpm FOREIGN key (updated_user_id) REFERENCES user (id);
  
ALTER TABLE 
  user_document 
ADD 
  CONSTRAINT FKyppkrojgqgjehui6u4r2oscn FOREIGN key (document_type_id) REFERENCES document_type (id);
  
ALTER TABLE 
  user_document 
ADD 
  CONSTRAINT FK8n7ck5r5gj5im1qrlleyp708n FOREIGN key (user_id) REFERENCES user (id);
  
ALTER TABLE 
  user_role 
ADD 
  CONSTRAINT FK26f1qdx6r8j1ggkgras9nrc1d FOREIGN key (role_id) REFERENCES role (id);
  
ALTER TABLE 
  user_role 
ADD 
  CONSTRAINT FKmow7bmkl6wduuutk26ypkgmm1 FOREIGN key (user_id) REFERENCES user (id);
