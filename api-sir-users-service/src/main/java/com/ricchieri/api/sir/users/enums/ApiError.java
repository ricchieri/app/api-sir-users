package com.ricchieri.api.sir.users.enums;

public enum ApiError {

    DUPLICATE_COMPANY(400_01_01, "Company already exists with pnr {0}"), COMPANY_NOT_FOUND(404_01_02,
            "Company not found"),;

    private int code;
    private String message;

    ApiError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("code=").append(code);
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
