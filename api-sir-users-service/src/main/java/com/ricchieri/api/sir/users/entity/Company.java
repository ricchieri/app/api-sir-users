package com.ricchieri.api.sir.users.entity;

import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "company")
public class Company extends AuditUser {

    private static final long serialVersionUID = -3933894427526812884L;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private List<CompanyDocument> documents;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ContactInformation contactInformation;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<User> users;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private List<CompanyComment> comments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CompanyDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<CompanyDocument> documents) {
        this.documents = documents;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<CompanyComment> getComments() {
        return comments;
    }

    public void setComments(List<CompanyComment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Company entity = (Company) o;
        return Objects.equals(getName(), entity.getName()) && Objects.equals(getDocuments(), entity.getDocuments())
                && Objects.equals(getContactInformation(), entity.getContactInformation())
                && Objects.equals(getUsers(), entity.getUsers()) && Objects.equals(getComments(), entity.getComments())
                && Objects.equals(getUpdatedUser(), entity.getUpdatedUser())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, documents, contactInformation, users, comments, getCreationUser(), getUpdatedUser(),
                getCreationDate(), getUpdatedDate(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "Company [id=" + getId() + "name=" + name + ", documents=" + documents + ", contactInformation="
                + contactInformation + "]";
    }
}
