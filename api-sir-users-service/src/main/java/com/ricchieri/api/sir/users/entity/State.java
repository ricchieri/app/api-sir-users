package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "state")
public class State extends Base {

    private static final long serialVersionUID = 1697687804373017457L;

    @Column(nullable = false, length = 100)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        State entity = (State) o;
        return Objects.equals(getName(), entity.getName()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "State [id=" + getId() + ", active=" + isActive() + ", optlock=" + getOptlock() + "name=" + getName()
                + "]";
    }
}