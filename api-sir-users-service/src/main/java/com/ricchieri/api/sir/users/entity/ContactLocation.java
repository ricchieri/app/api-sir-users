package com.ricchieri.api.sir.users.entity;

import java.util.Comparator;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contact_location")
public class ContactLocation extends AuditUser {

    private static final long serialVersionUID = -887992292808613737L;

    @Column(nullable = false, length = 250)
    private String street;

    @Column(nullable = true, length = 50)
    private String zipcode;

    @Column(nullable = false, length = 250)
    private String city;

    @Column(nullable = true, length = 50)
    private String floor;

    @Column(nullable = true, length = 50)
    private String suite;

    @ManyToOne(fetch = FetchType.LAZY)
    private State state;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getFloor() {
        return floor;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ContactLocation entity = (ContactLocation) o;
        return Objects.equals(getCity(), entity.getCity()) && Objects.equals(getFloor(), entity.getFloor())
                && Objects.equals(getStreet(), entity.getStreet()) && Objects.equals(getSuite(), entity.getSuite())
                && Objects.equals(getState(), entity.getState())
                && Objects.equals(getUpdatedUser(), entity.getUpdatedUser())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCity(), getFloor(), getStreet(), getSuite(), getState(), getCreationUser(),
                getUpdatedUser(), getCreationDate(), getUpdatedDate(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "ContactLocation [id=" + getId() + "city=" + getCity() + ", floor=" + getFloor() + ", street="
                + getStreet() + ", suite=" + getSuite() + ", state=" + getState() + "]";
    }

    public static Comparator<ContactLocation> getComparator() {
        return new Comparator<ContactLocation>() {
            @Override
            public int compare(ContactLocation o1, ContactLocation o2) {
                int result;
                if (o1.getStreet() != null && o2.getStreet() != null) {
                    result = o1.getStreet().compareTo(o2.getStreet());
                } else if (o1.getStreet() != null) {
                    result = 1;
                } else if (o2.getStreet() != null) {
                    result = 1;
                } else {
                    result = 0;
                }
                return result;
            }
        };
    }
}