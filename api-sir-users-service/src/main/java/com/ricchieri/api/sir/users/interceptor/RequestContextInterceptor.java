package com.ricchieri.api.sir.users.interceptor;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.ModelAndView;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import com.ricchieri.api.sir.users.constants.HeaderConstants;
import com.ricchieri.api.sir.users.constants.LoggingConstants;
import com.ricchieri.api.sir.users.context.RequestContext;
import com.ricchieri.api.sir.users.context.RequestContextDTO;

public class RequestContextInterceptor extends HandlerInterceptorAdapter {

    private final RequestContext requestContext;

    public RequestContextInterceptor(final RequestContext requestContext) {
        this.requestContext = requestContext;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        final RequestContextDTO requestContextDTO = buildRequestContextDTO(request);

        requestContext.setContext(requestContextDTO);

        setParamsForLogging(requestContextDTO);

        return super.preHandle(request, response, handler);
    }

    protected void setParamsForLogging(RequestContextDTO requestContextDTO) {
        MDC.put(LoggingConstants.CHANNEL, requestContextDTO.getChannel());
        MDC.put(LoggingConstants.USER_ID, requestContextDTO.getUserId());
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {

        requestContext.clearContext();
        super.postHandle(request, response, handler, modelAndView);
    }

    protected RequestContextDTO buildRequestContextDTO(HttpServletRequest request) {
        String channel = getValue(request, HeaderConstants.HEADER_CHANNEL);
        String userId = getValue(request, HeaderConstants.HEADER_USER_ID);

        RequestContextDTO requestContextDto = new RequestContextDTO();
        requestContextDto.setChannel(channel);
        requestContextDto.setUserId(userId);

        Enumeration<String> headerNames = request.getHeaderNames();

        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {

                String name = headerNames.nextElement();
                requestContextDto.getHeaders().put(name, request.getHeader(name));
            }
        }

        return requestContextDto;
    }

    protected String getValue(HttpServletRequest request, String param) {
        String value = request.getHeader(param);
        if (!StringUtils.isEmpty(value)) {
            return value.toUpperCase();
        }

        return null;
    }

}
