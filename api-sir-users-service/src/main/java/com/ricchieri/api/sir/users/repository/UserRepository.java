package com.ricchieri.api.sir.users.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ricchieri.api.sir.users.entity.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

}
