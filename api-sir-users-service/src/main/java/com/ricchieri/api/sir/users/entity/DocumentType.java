package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "document_type")
public class DocumentType extends Base {

    private static final long serialVersionUID = 6060708418479383984L;

    @Column(name = "description", nullable = false, length = 50)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        DocumentType audit = (DocumentType) o;
        return Objects.equals(getDescription(), audit.getDescription()) && Objects.equals(getId(), audit.getId())
                && Objects.equals(isActive(), audit.isActive()) && Objects.equals(getOptlock(), audit.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDescription(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "DocumentType [id=" + getId() + ", active=" + isActive() + ", optlock=" + getOptlock() + "description="
                + getDescription() + "]";
    }

}
