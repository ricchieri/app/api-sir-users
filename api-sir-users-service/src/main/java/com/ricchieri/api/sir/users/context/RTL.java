package com.ricchieri.api.sir.users.context;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.rits.cloning.Cloner;

public class RTL {

    private static ThreadLocal<Map<Object, RTLValueWrapper>> dataThreadLocal = new ThreadLocal<>();

    public static void put(Object key, Object value) {
        put(key, value, true);
    }

    public static void put(Object key, Object value, boolean inherithedAsCopy) {
        Map<Object, RTLValueWrapper> threadMap = dataThreadLocal.get();

        if (threadMap == null) {
            threadMap = new ConcurrentHashMap<>();
            dataThreadLocal.set(threadMap);
        }

        threadMap.put(key, new RTLValueWrapper(inherithedAsCopy, value));
    }

    public static Object get(Object key) {

        Map<Object, RTLValueWrapper> threadMap = dataThreadLocal.get();

        if (threadMap == null || threadMap.get(key) == null) {
            return null;
        }

        return threadMap.get(key).getValue();
    }

    public static void remove(Object key) {
        dataThreadLocal.get().remove(key);
    }

    public static void clear() {
        Map<Object, RTLValueWrapper> threadMap = dataThreadLocal.get();

        if (threadMap != null) {
            dataThreadLocal.get().clear();
        }
    }

    public static void destroy() {
        Map<Object, RTLValueWrapper> threadMap = dataThreadLocal.get();

        if (threadMap != null) {
            dataThreadLocal.remove();
        }
    }

    public static Map<Object, RTLValueWrapper> getDataCopy() {

        Map<Object, RTLValueWrapper> threadMap = dataThreadLocal.get();

        if (threadMap != null) {
            Cloner cloner = new Cloner();

            Map<Object, RTLValueWrapper> threadMapCopy = new HashMap<>();

            threadMap.entrySet().forEach(entry -> {
                if (entry.getValue().isInheritedAsCopy()) {
                    threadMapCopy.put(entry.getKey(), cloner.deepClone(entry.getValue()));
                } else {
                    threadMapCopy.put(entry.getKey(), entry.getValue());
                }
            });

            return threadMapCopy;
        }

        return null;
    }
}
