package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "contact_email")
public class ContactEmail extends Audit {
    private static final long serialVersionUID = -7467185688422338869L;

    @Column(name = "email", nullable = false, length = 80)
    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    private EmailType emailType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "is_default")
    private Boolean isDefault = Boolean.FALSE;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EmailType getEmailType() {
        return emailType;
    }

    public void setEmailType(EmailType emailType) {
        this.emailType = emailType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ContactEmail entity = (ContactEmail) o;
        return Objects.equals(getEmail(), entity.getEmail()) && Objects.equals(getEmailType(), entity.getEmailType())
                && Objects.equals(getUser(), entity.getUser()) && Objects.equals(getIsDefault(), entity.getIsDefault())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail(), getEmailType(), getUser(), getIsDefault(), getCreationDate(), getUpdatedDate(),
                getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "ContactEmail [id=" + getId() + "email=" + getEmail() + ", emailType=" + getEmailType() + ", user="
                + getUser() + ", isDefault=" + getIsDefault() + "]";
    }

}