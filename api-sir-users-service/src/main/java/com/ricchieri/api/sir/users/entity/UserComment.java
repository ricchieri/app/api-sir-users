package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_comments")
public class UserComment extends Comment {

    private static final long serialVersionUID = -6316618818574945595L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserComment entity = (UserComment) o;
        return Objects.equals(getUser(), entity.getUser()) && Objects.equals(getMessage(), entity.getMessage())
                && Objects.equals(getUpdatedUser(), entity.getUpdatedUser())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), getMessage(), getCreationUser(), getUpdatedUser(), getCreationDate(),
                getUpdatedDate(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "UserComment [user=" + getUser() + "message=" + getMessage() + "]";
    }
}