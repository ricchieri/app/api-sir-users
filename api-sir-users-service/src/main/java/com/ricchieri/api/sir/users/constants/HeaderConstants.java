package com.ricchieri.api.sir.users.constants;

public class HeaderConstants {

    public static final String HEADER_PREFIX = "X-SIR-";
    public static final String HEADER_REQUEST_ID = HEADER_PREFIX + "RequestId";
    public static final String HEADER_CLIENT_REAL_IP = HEADER_PREFIX + "RealIP";
    public static final String HEADER_CLIENT_APP_NAME_SUFFIX = "ClientAppName";
    public static final String HEADER_CLIENT_APP_NAME = HEADER_PREFIX + HEADER_CLIENT_APP_NAME_SUFFIX;
    public static final String CDN_HEADER_CLIENT_REAL_IP = "True-Client-IP";

    public static final String HEADER_CHANNEL = HEADER_PREFIX + "Channel";
    public static final String HEADER_USER_ID = HEADER_PREFIX + "User-Id";
}
