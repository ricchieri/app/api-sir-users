package com.ricchieri.api.sir.users.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public abstract class Audit extends Base {

    private static final long serialVersionUID = -3895623229183524677L;

    @Column(name = "creation_date", nullable = false)
    private Date creationDate;

    @Column(name = "updated_date", nullable = true)
    private Date updatedDate;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @PrePersist
    void onCreate() {
        this.setCreationDate(new Date());
    }

    @PreUpdate
    void onUpdate() {
        this.setUpdatedDate(new Date());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Audit audit = (Audit) o;
        return Objects.equals(creationDate, audit.creationDate) && Objects.equals(updatedDate, audit.updatedDate)
                && Objects.equals(getId(), audit.getId()) && Objects.equals(isActive(), audit.isActive())
                && Objects.equals(getOptlock(), audit.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationDate, updatedDate, getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "Audit [id=" + getId() + ", active=" + isActive() + ", optlock=" + getOptlock() + ", creationDate="
                + creationDate + ", updatedDate=" + updatedDate + "]";
    }

}