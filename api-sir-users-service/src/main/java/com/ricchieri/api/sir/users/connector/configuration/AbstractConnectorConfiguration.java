package com.ricchieri.api.sir.users.connector.configuration;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import com.ricchieri.api.sir.users.constants.HeaderConstants;
import com.ricchieri.api.sir.users.context.RequestContext;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

public abstract class AbstractConnectorConfiguration {

    private static final String ENCODING_GZIP = "gzip";

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractConnectorConfiguration.class);

    @Autowired
    private RequestContext requestContext;

    private String host;
    private String protocol;
    private Integer connectionTimeout;
    private Integer readTimeout;
    private Integer writeTimeout;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Integer getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Integer readTimeout) {
        this.readTimeout = readTimeout;
    }

    public Integer getWriteTimeout() {
        return writeTimeout;
    }

    public void setWriteTimeout(Integer writeTimeout) {
        this.writeTimeout = writeTimeout;
    }

    public RequestContext getRequestContext() {
        return requestContext;
    }

    public void setRequestContext(RequestContext requestContext) {
        this.requestContext = requestContext;
    }

    protected WebClient createWebClient() {

        HttpClient httpClient = HttpClient.create()
                .tcpConfiguration(
                        client -> client.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, getConnectionTimeout())
                                .doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(getReadTimeout()))
                                        .addHandlerLast(new WriteTimeoutHandler(getWriteTimeout()))))
                .compress(Boolean.TRUE);

        ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient.wiretap(Boolean.TRUE));

        return WebClient.builder().baseUrl(protocol.concat("://").concat(host))
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT_ENCODING, ENCODING_GZIP).filter(addCustomHeaders())
                .filter(logRequest()).filter(logResponse()).clientConnector(connector).build();
    }

    private ExchangeFilterFunction addCustomHeaders() {
        return (clientRequest, next) -> {
            if (requestContext.getHeaders() != null) {
                for (Map.Entry<String, String> entry : requestContext.getHeaders().entrySet()) {
                    requestContext.getHeaders().put(entry.getKey(), entry.getValue());
                }
            }
            requestContext.getHeaders().put(HeaderConstants.HEADER_CLIENT_APP_NAME, getAppName());
            return next.exchange(clientRequest);
        };
    }

    private ExchangeFilterFunction logRequest() {

        return (clientRequest, next) -> {
            LOGGER.debug("Request: {} {}", clientRequest.method(), clientRequest.url());
            LOGGER.debug("--- Http Headers: ---");
            clientRequest.headers().forEach(this::logHeader);
            LOGGER.debug("--- Http Cookies: ---");
            clientRequest.cookies().forEach(this::logHeader);
            return next.exchange(clientRequest);
        };
    }

    private ExchangeFilterFunction logResponse() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
            LOGGER.debug("Response: {}", clientResponse.statusCode());
            clientResponse.headers().asHttpHeaders()
                    .forEach((name, values) -> values.forEach(value -> LOGGER.debug("{}={}", name, value)));
            return Mono.just(clientResponse);
        });
    }

    private void logHeader(String name, List<String> values) {
        values.forEach(value -> LOGGER.debug("{}={}", name, value));
    }

    protected abstract String getAppName();

}
