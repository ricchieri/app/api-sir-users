package com.ricchieri.api.sir.users.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "user_role")
public class UserRole extends Audit {

    private static final long serialVersionUID = 9068935268439311788L;

    @Column(name = "start_date", nullable = false)
    private Date start;

    @Column(name = "finish_date", nullable = true)
    private Date finish;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Role role;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getFinish() {
        return finish;
    }

    public void setFinish(Date finish) {
        this.finish = finish;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserRole entity = (UserRole) o;
        return Objects.equals(getStart(), entity.getStart()) && Objects.equals(getFinish(), entity.getFinish())
                && Objects.equals(getUser(), entity.getUser()) && Objects.equals(getRole(), entity.getRole())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStart(), getFinish(), getUser(), getRole(), getCreationDate(), getUpdatedDate(), getId(),
                isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "UserRole [id=" + getId() + "start=" + getStart() + ", finish=" + getFinish() + ", user=" + getUser()
                + ", role=" + getRole() + "]";
    }

}