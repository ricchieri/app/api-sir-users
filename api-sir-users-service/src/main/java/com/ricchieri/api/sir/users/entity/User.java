package com.ricchieri.api.sir.users.entity;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "user")
public class User extends Base {

    private static final long serialVersionUID = 6220080725771220681L;

    @Column(name = "first_name", nullable = false, length = 100)
    private String firstname;

    @Column(name = "last_name", nullable = false, length = 100)
    private String lastname;

    @Column(name = "birthday", nullable = true)
    private Date birthday;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<UserDocument> documents;

    @Column(name = "username", nullable = false, length = 11)
    private String username;

    @Column(name = "password", nullable = false, length = 256)
    private String password;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<UserComment> comments;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<UserRole> roles;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ContactInformation contactInformation;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserExtraInformation extraInformation;

    @Column(name = "login_attemp", nullable = false)
    private int loginAttemp = 0;

    @Column(name = "last_login", nullable = true)
    private Date lastLogin;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<UserDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<UserDocument> documents) {
        this.documents = documents;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserComment> getComments() {
        return comments;
    }

    public void setComments(List<UserComment> comments) {
        this.comments = comments;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    public UserExtraInformation getExtraInformation() {
        return extraInformation;
    }

    public void setExtraInformation(UserExtraInformation extraInformation) {
        this.extraInformation = extraInformation;
    }

    public int getLoginAttemp() {
        return loginAttemp;
    }

    public void setLoginAttemp(int loginAttemp) {
        this.loginAttemp = loginAttemp;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        User entity = (User) o;
        return Objects.equals(getFirstname(), entity.getFirstname())
                && Objects.equals(getLastname(), entity.getLastname())
                && Objects.equals(getBirthday(), entity.getBirthday())
                && Objects.equals(getDocuments(), entity.getDocuments())
                && Objects.equals(getUsername(), entity.getUsername())
                && Objects.equals(getPassword(), entity.getPassword())
                && Objects.equals(getComments(), entity.getComments()) && Objects.equals(getRoles(), entity.getRoles())
                && Objects.equals(getContactInformation(), entity.getContactInformation())
                && Objects.equals(getExtraInformation(), entity.getExtraInformation())
                && Objects.equals(getLoginAttemp(), entity.getLoginAttemp())
                && Objects.equals(getLastLogin(), entity.getLastLogin()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstname(), getLastname(), getBirthday(), getDocuments(), getUsername(), getPassword(),
                getComments(), getRoles(), getContactInformation(), getExtraInformation(), getLoginAttemp(),
                getLastLogin(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "User [id=" + getId() + ", firstname=" + firstname + ", lastname=" + lastname + ", birthday=" + birthday
                + ", documents=" + documents + ", username=" + username + ", password=" + password + ", comments="
                + comments + ", roles=" + roles + ", contactInformation=" + contactInformation + ", extraInformation="
                + extraInformation + ", loginAttemp=" + loginAttemp + ", lastLogin=" + lastLogin + "]";
    }

}