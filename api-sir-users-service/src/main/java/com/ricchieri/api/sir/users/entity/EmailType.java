package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "email_type")
public class EmailType extends Base {
    private static final long serialVersionUID = 1194205251045136444L;

    @Column(name = "description", nullable = false, length = 50)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        EmailType entity = (EmailType) o;
        return Objects.equals(getDescription(), entity.getDescription()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDescription(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "EmailType [id=" + getId() + ", active=" + isActive() + ", optlock=" + getOptlock() + "description="
                + getDescription() + "]";
    }
}