package com.ricchieri.api.sir.users.controller;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ricchieri.api.sir.dto.CompanyDTO;
import com.ricchieri.api.sir.users.controller.documentation.CompanyResource;
import com.ricchieri.api.sir.users.request.SearchRequest;

import io.swagger.annotations.Api;

@Api(tags = { "Companies" })
@RestController
@RequestMapping(path = "company")
public class CompanyController implements CompanyResource {

    @Override
    public ResponseEntity<Void> delete(final String companyId) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CompanyDTO> get(final String companyId) {
        return new ResponseEntity<>(new CompanyDTO(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page<CompanyDTO>> getFilter(SearchRequest searchRequest) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CompanyDTO> put(CompanyDTO company) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CompanyDTO> post(CompanyDTO company) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
