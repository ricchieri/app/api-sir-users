package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_extra_information")
public class UserExtraInformation extends Audit {

    private static final long serialVersionUID = 4940049518900693828L;

    @Column(name = "block", nullable = false)
    private Boolean block = Boolean.FALSE;

    @Column(name = "block_motive", nullable = true, columnDefinition = "TEXT")
    private String blockMotive;

    @Column(name = "current_account", nullable = false)
    private Boolean currentAccount = Boolean.FALSE;

    @Column(name = "cancellation_charge", nullable = false)
    private Boolean cancellationCharge = Boolean.TRUE;

    @Column(name = "office_id", nullable = false)
    private Long officeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    public String getBlockMotive() {
        return blockMotive;
    }

    public void setBlockMotive(String blockMotive) {
        this.blockMotive = blockMotive;
    }

    public Boolean getCurrentAccount() {
        return currentAccount;
    }

    public void setCurrentAccount(Boolean currentAccount) {
        this.currentAccount = currentAccount;
    }

    public Boolean getCancellationCharge() {
        return cancellationCharge;
    }

    public void setCancellationCharge(Boolean cancellationCharge) {
        this.cancellationCharge = cancellationCharge;
    }

    public Long getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Long officeId) {
        this.officeId = officeId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserExtraInformation entity = (UserExtraInformation) o;
        return Objects.equals(getBlock(), entity.getBlock())
                && Objects.equals(getBlockMotive(), entity.getBlockMotive())
                && Objects.equals(getCurrentAccount(), entity.getCurrentAccount())
                && Objects.equals(getCancellationCharge(), entity.getCancellationCharge())
                && Objects.equals(getOfficeId(), entity.getOfficeId()) && Objects.equals(getUser(), entity.getUser())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBlock(), getBlockMotive(), getCurrentAccount(), getOfficeId(), getUser(),
                getCreationDate(), getUpdatedDate(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "UserExtraInformation [id=" + getId() + "block=" + getBlock() + ", blockMotive=" + getBlockMotive()
                + ", currentAccount=" + getCurrentAccount() + ", cancellationCharge=" + getCancellationCharge()
                + ", officeId=" + getOfficeId() + ", user=" + getUser() + "]";
    }
}
