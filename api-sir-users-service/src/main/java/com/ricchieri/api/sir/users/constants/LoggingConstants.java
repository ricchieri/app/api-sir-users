package com.ricchieri.api.sir.users.constants;

public class LoggingConstants {
    public static final String CHANNEL = "channel";
    public static final String USER_ID = "userId";
}
