package com.ricchieri.api.sir.users.context;

import java.util.HashMap;
import java.util.Map;

public class RequestContextDTO {

    private Map<String, String> headers = new HashMap<>();

    private String userId;

    private String channel;

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
