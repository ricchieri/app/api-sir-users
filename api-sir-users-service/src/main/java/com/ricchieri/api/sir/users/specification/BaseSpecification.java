package com.ricchieri.api.sir.users.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.ricchieri.api.sir.users.entity.Base;
import com.ricchieri.api.sir.users.request.SearchCriteria;

public class BaseSpecification implements Specification<Base> {

    private static final long serialVersionUID = 2812420046016941005L;

    private SearchCriteria criteria;

    public BaseSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<Base> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        switch (criteria.getOperation()) {
        case CONTAINS:
            return builder.like(root.<String> get(criteria.getKey()), "%" + criteria.getValue() + "%");
        case STARTS_WITH:
            return builder.like(root.get(criteria.getKey()), criteria.getValue() + "%");
        case ENDS_WITH:
            return builder.like(root.get(criteria.getKey()), "%" + criteria.getValue());
        case NEGATION:
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return builder.notEqual(root.<String> get(criteria.getKey()), criteria.getValue());
            } else {
                return builder.notEqual(root.get(criteria.getKey()), criteria.getValue());
            }
        case EQUALS:
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return builder.equal(root.<String> get(criteria.getKey()), criteria.getValue());
            } else {
                return builder.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        case GREATER_THAN:
            return builder.greaterThan(root.<String> get(criteria.getKey()), criteria.getValue().toString());
        case GREATER_OR_EQUAL_THAN:
            return builder.greaterThanOrEqualTo(root.<String> get(criteria.getKey()), criteria.getValue().toString());
        case LESS_THAN:
            return builder.lessThan(root.<String> get(criteria.getKey()), criteria.getValue().toString());
        case LESS_OR_EQUAL_THAN:
            return builder.lessThanOrEqualTo(root.<String> get(criteria.getKey()), criteria.getValue().toString());
        default:
            return null;
        }
    }

}
