package com.ricchieri.api.sir.users.util;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.ricchieri.api.sir.users.configuration.CacheConfiguration;

import org.springframework.cache.caffeine.CaffeineCache;

import java.util.function.Function;
import java.util.function.Supplier;

public final class CaffeineCacheHelper {

    private CaffeineCacheHelper() {

    }

    public static CaffeineCache buildLoadingCache(String name, CacheConfiguration cacheConfiguration,
            Function<String, Object> cacheLoader) {

        return createCaffeineCache(name,
                Caffeine.newBuilder()
                        .refreshAfterWrite(cacheConfiguration.getDuration(), cacheConfiguration.getTimeUnit())
                        .maximumSize(cacheConfiguration.getMaxSize()).build(key -> cacheLoader.apply(key.toString())));
    }

    public static CaffeineCache buildLoadingCache(String name, CacheConfiguration cacheConfiguration,
            Supplier<Object> cacheLoader) {

        return createCaffeineCache(name,
                Caffeine.newBuilder()
                        .refreshAfterWrite(cacheConfiguration.getDuration(), cacheConfiguration.getTimeUnit())
                        .maximumSize(cacheConfiguration.getMaxSize()).build(key -> cacheLoader.get()));
    }

    public static CaffeineCache createCaffeineCache(String name, Cache<Object, Object> cache) {
        return new CaffeineCache(name, cache);
    }

}
