package com.ricchieri.api.sir.users.controller;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ricchieri.api.sir.dto.CompanyCommentDTO;
import com.ricchieri.api.sir.users.controller.documentation.CompanyCommentResource;
import com.ricchieri.api.sir.users.request.SearchRequest;

import io.swagger.annotations.Api;

@Api(tags = { "Comments of the company" })
@RestController
@RequestMapping(path = "companyComment")
public class CompanyCommentController implements CompanyCommentResource {

    @Override
    public ResponseEntity<Void> delete(final String companyCommentId) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CompanyCommentDTO> get(final String companyCommentId) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page<CompanyCommentDTO>> getFilter(SearchRequest searchRequest) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CompanyCommentDTO> put(CompanyCommentDTO companyComment) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CompanyCommentDTO> post(CompanyCommentDTO companyComment) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
