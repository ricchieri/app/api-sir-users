package com.ricchieri.api.sir.users.entity;

import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "role")
public class Role extends Base {

    private static final long serialVersionUID = -5042110089935168786L;

    @Column(name = "description", nullable = false, length = 50)
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Permission> permissions;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Role entity = (Role) o;
        return Objects.equals(getDescription(), entity.getDescription())
                && Objects.equals(getPermissions(), entity.getPermissions()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDescription(), getPermissions(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "Role [id=" + getId() + "description=" + getDescription() + ", permissions=" + getPermissions() + "]";
    }
}