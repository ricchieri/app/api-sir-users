package com.ricchieri.api.sir.users.context;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Component
public class RequestContext {

    private static final String REQUEST_CONTEXT_KEY = "REQUEST_CONTEXT_KEY";

    public Map<String, String> getHeaders() {
        return getCurrentRequestContext().getHeaders();
    }

    public String getUserId() {
        return getCurrentRequestContext().getUserId();
    }

    public String getChannel() {
        return getCurrentRequestContext().getChannel();
    }

    public RequestContextDTO getCurrentRequestContext() {
        return Optional.ofNullable((RequestContextDTO) RTL.get(REQUEST_CONTEXT_KEY)).orElse(new RequestContextDTO());
    }

    public void clearContext() {
        if (!Objects.isNull(RTL.get(REQUEST_CONTEXT_KEY))) {
            RTL.remove(REQUEST_CONTEXT_KEY);
        }
    }

    public void setContext(RequestContextDTO requestContextDTO) {
        RTL.put(REQUEST_CONTEXT_KEY, requestContextDTO);
    }
}
