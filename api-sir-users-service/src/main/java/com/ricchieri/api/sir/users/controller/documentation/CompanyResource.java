package com.ricchieri.api.sir.users.controller.documentation;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ricchieri.api.sir.dto.CompanyDTO;
import com.ricchieri.api.sir.users.request.SearchRequest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface CompanyResource {

    @ApiOperation(value = "Endpoint which delete one company")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "if the company was delete", response = Void.class),
            @ApiResponse(code = 400, message = "Invalid companyId") })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", dataType = "java.lang.String", required = true, paramType = "path", value = "indicate the id of the company") })
    @DeleteMapping(value = "{companyId}")
    @ResponseBody
    ResponseEntity<Void> delete(@PathVariable final String companyId);

    @ApiOperation(value = "Endpoint which retrieve all the information for one company")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "if the company exists", response = CompanyDTO.class),
            @ApiResponse(code = 400, message = "Invalid companyId") })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", dataType = "java.lang.String", required = true, paramType = "path", value = "indicate the id of the company") })
    @GetMapping(value = "{companyId}")
    @ResponseBody
    ResponseEntity<CompanyDTO> get(@PathVariable final String companyId);

    @ApiOperation(value = "Endpoint which retrieve all the information for all companies")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "if the companies exists", response = Page.class),
            @ApiResponse(code = 400, message = "Something happen with the filter") })
    @GetMapping
    @ResponseBody
    ResponseEntity<Page<CompanyDTO>> getFilter(@RequestParam final SearchRequest searchRequest);

    @ApiOperation(value = "Endpoint which update all the information for one company")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "update all the fields of the company", response = CompanyDTO.class),
            @ApiResponse(code = 404, message = "Company not found") })
    @PutMapping
    @ResponseBody
    ResponseEntity<CompanyDTO> put(@RequestBody final CompanyDTO company);

    @ApiOperation(value = "Endpoint which create one company")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "create one company", response = CompanyDTO.class),
            @ApiResponse(code = 400, message = "Company with bad information or duplicated") })
    @PostMapping
    @ResponseBody
    ResponseEntity<CompanyDTO> post(@RequestBody final CompanyDTO company);

}
