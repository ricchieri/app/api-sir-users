package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "contact_phone")
public class ContactPhone extends Audit {
    private static final long serialVersionUID = -5103517844173568696L;

    @Column(name = "number", nullable = false, length = 30)
    private String number;

    @ManyToOne(fetch = FetchType.LAZY)
    private PhoneType phoneType;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column(name = "is_default")
    private Boolean isDefault = Boolean.FALSE;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PhoneType getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ContactPhone entity = (ContactPhone) o;
        return Objects.equals(getNumber(), entity.getNumber()) && Objects.equals(getPhoneType(), entity.getPhoneType())
                && Objects.equals(getUser(), entity.getUser()) && Objects.equals(getIsDefault(), entity.getIsDefault())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumber(), getPhoneType(), getUser(), getIsDefault(), getCreationDate(), getUpdatedDate(),
                getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "ContactPhone [id=" + getId() + "number=" + getNumber() + ", phoneType=" + getPhoneType() + ", user="
                + getUser() + ", isDefault=" + getIsDefault() + "]";
    }

}