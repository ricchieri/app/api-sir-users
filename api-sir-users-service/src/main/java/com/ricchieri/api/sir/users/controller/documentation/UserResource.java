package com.ricchieri.api.sir.users.controller.documentation;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ricchieri.api.sir.dto.UserDTO;
import com.ricchieri.api.sir.users.request.SearchRequest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface UserResource {

    @ApiOperation(value = "Endpoint which delete one user")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "if the user was delete", response = Void.class),
            @ApiResponse(code = 400, message = "Invalid userId") })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", dataType = "java.lang.String", required = true, paramType = "path", value = "indicate the id of the user") })
    @DeleteMapping(value = "{userId}")
    @ResponseBody
    ResponseEntity<Void> delete(@PathVariable final String userId);

    @ApiOperation(value = "Endpoint which retrieve all the information for one user")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "if the user exists", response = UserDTO.class),
            @ApiResponse(code = 400, message = "Invalid userId") })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", dataType = "java.lang.String", required = true, paramType = "path", value = "indicate the id of the user") })
    @GetMapping(value = "{userId}")
    @ResponseBody
    ResponseEntity<UserDTO> get(@PathVariable final String userId);

    @ApiOperation(value = "Endpoint which retrieve all the information for all companies")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "if the users exists", response = Page.class),
            @ApiResponse(code = 400, message = "Something happen with the filter") })
    @GetMapping
    @ResponseBody
    ResponseEntity<Page<UserDTO>> getFilter(@RequestParam final SearchRequest searchRequest);

    @ApiOperation(value = "Endpoint which update all the information for one user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "update all the fields of the user", response = UserDTO.class),
            @ApiResponse(code = 404, message = "User not found") })
    @PutMapping
    @ResponseBody
    ResponseEntity<UserDTO> put(@RequestBody final UserDTO user);

    @ApiOperation(value = "Endpoint which create one user")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "create one user", response = UserDTO.class),
            @ApiResponse(code = 400, message = "User with bad information or duplicated") })
    @PostMapping
    @ResponseBody
    ResponseEntity<UserDTO> post(@RequestBody final UserDTO user);

}
