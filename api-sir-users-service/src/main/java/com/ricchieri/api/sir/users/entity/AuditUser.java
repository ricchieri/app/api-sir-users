package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AuditUser extends Audit {

    private static final long serialVersionUID = 6830730673057658599L;

    @ManyToOne(fetch = FetchType.LAZY)
    private User creationUser;

    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedUser;

    public User getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(User creationUser) {
        this.creationUser = creationUser;
    }

    public User getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(User updatedUser) {
        this.updatedUser = updatedUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AuditUser audit = (AuditUser) o;
        return Objects.equals(getCreationUser(), audit.getCreationUser())
                && Objects.equals(getUpdatedUser(), audit.getUpdatedUser())
                && Objects.equals(getCreationDate(), audit.getCreationDate())
                && Objects.equals(getUpdatedDate(), audit.getUpdatedDate()) && Objects.equals(getId(), audit.getId())
                && Objects.equals(isActive(), audit.isActive()) && Objects.equals(getOptlock(), audit.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationUser, updatedUser, getCreationDate(), getUpdatedDate(), getId(), isActive(),
                getOptlock());
    }

    @Override
    public String toString() {
        return "AuditUser [id=" + getId() + ", active=" + isActive() + ", optlock=" + getOptlock() + "creationDate="
                + getCreationDate() + ", updatedDate=" + getUpdatedDate() + "creationUser=" + creationUser
                + "updatedUser=" + updatedUser + "]";
    }
}