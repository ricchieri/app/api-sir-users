package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@MappedSuperclass
public abstract class Document extends AuditUser {

    private static final long serialVersionUID = 4074547810692808739L;

    @Column(name = "document_number", nullable = false, length = 15)
    private String documentNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_type_id", nullable = false)
    private DocumentType documentType;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Document entity = (Document) o;
        return Objects.equals(getDocumentNumber(), entity.getDocumentNumber())
                && Objects.equals(getDocumentType(), entity.getDocumentType())
                && Objects.equals(getCreationUser(), entity.getCreationUser())
                && Objects.equals(getUpdatedUser(), entity.getUpdatedUser())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDocumentNumber(), getDocumentType(), getCreationUser(), getUpdatedUser(),
                getCreationDate(), getUpdatedDate(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "Document [id=" + getId() + ", active=" + isActive() + ", optlock=" + getOptlock() + "creationDate="
                + getCreationDate() + ", updatedDate=" + getUpdatedDate() + "creationUser=" + getCreationUser()
                + "updatedUser=" + getUpdatedUser() + ", documentNumber=" + getDocumentNumber() + "documentType="
                + getDocumentType() + "]";
    }
}
