package com.ricchieri.api.sir.users.controller.documentation;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ricchieri.api.sir.dto.CompanyCommentDTO;
import com.ricchieri.api.sir.dto.CompanyDTO;
import com.ricchieri.api.sir.users.request.SearchRequest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface CompanyCommentResource {

    @ApiOperation(value = "Endpoint which delete one comment of the one company")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "if the comment was delete", response = Void.class),
            @ApiResponse(code = 400, message = "Invalid companyId") })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyCommentId", dataType = "java.lang.String", required = true, paramType = "path", value = "indicate the id of the comment") })
    @DeleteMapping(value = "{companyCommentId}")
    @ResponseBody
    ResponseEntity<Void> delete(@PathVariable final String companyCommentId);

    @ApiOperation(value = "Endpoint which retrieve all the information for one comment of the company")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "if the company exists", response = CompanyCommentDTO.class),
            @ApiResponse(code = 400, message = "Invalid companyId") })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyCommentId", dataType = "java.lang.String", required = true, paramType = "path", value = "indicate the id of the comment") })
    @GetMapping(value = "{companyCommentId}")
    @ResponseBody
    ResponseEntity<CompanyCommentDTO> get(@PathVariable final String companyCommentId);

    @ApiOperation(value = "Endpoint which retrieve all the information for all comments")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "if the comments exists", response = Page.class),
            @ApiResponse(code = 400, message = "Something happen with the filter") })
    @GetMapping
    @ResponseBody
    ResponseEntity<Page<CompanyCommentDTO>> getFilter(@RequestParam final SearchRequest searchRequest);

    @ApiOperation(value = "Endpoint which update all the information for one comment")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "update all the fields of the comment", response = CompanyCommentDTO.class),
            @ApiResponse(code = 404, message = "comment not found") })
    @PutMapping
    @ResponseBody
    ResponseEntity<CompanyCommentDTO> put(@RequestBody final CompanyCommentDTO company);

    @ApiOperation(value = "Endpoint which create one comment of the company")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "create one comment", response = CompanyDTO.class),
            @ApiResponse(code = 400, message = "Comment with bad information or duplicated") })
    @PostMapping
    @ResponseBody
    ResponseEntity<CompanyCommentDTO> post(@RequestBody final CompanyCommentDTO company);
}
