package com.ricchieri.api.sir.users.entity;

import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "contact_information")
public class ContactInformation extends AuditUser {

    private static final long serialVersionUID = 1015703352878989362L;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private List<ContactPhone> phones;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private List<ContactEmail> emails;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ContactLocation location;

    public List<ContactPhone> getPhones() {
        return phones;
    }

    public void setPhones(List<ContactPhone> phones) {
        this.phones = phones;
    }

    public List<ContactEmail> getEmails() {
        return emails;
    }

    public void setEmails(List<ContactEmail> emails) {
        this.emails = emails;
    }

    public ContactLocation getLocation() {
        return location;
    }

    public void setLocation(ContactLocation location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ContactInformation entity = (ContactInformation) o;
        return Objects.equals(getPhones(), entity.getPhones()) && Objects.equals(getEmails(), entity.getEmails())
                && Objects.equals(getLocation(), entity.getLocation())
                && Objects.equals(getUpdatedUser(), entity.getUpdatedUser())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPhones(), getEmails(), getLocation(), getCreationUser(), getUpdatedUser(),
                getCreationDate(), getUpdatedDate(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "ContactInformation [id=" + getId() + "phones=" + getPhones() + ", emails=" + getEmails() + ", location="
                + getLocation() + "]";
    }
}
