package com.ricchieri.api.sir.users.controller;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ricchieri.api.sir.dto.UserDTO;
import com.ricchieri.api.sir.users.controller.documentation.UserResource;
import com.ricchieri.api.sir.users.request.SearchRequest;

import io.swagger.annotations.Api;

@Api(tags = { "Users" })
@RestController
@RequestMapping(path = "user")
public class UserController implements UserResource {

    @Override
    public ResponseEntity<Void> delete(final String userId) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDTO> get(final String userId) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page<UserDTO>> getFilter(SearchRequest searchRequest) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDTO> put(UserDTO company) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDTO> post(UserDTO company) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
