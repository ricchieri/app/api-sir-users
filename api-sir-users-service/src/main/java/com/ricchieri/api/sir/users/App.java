package com.ricchieri.api.sir.users;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.actuate.info.InfoEndpoint;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.yaml.snakeyaml.Yaml;

import com.ricchieri.api.sir.users.context.RequestContext;
import com.ricchieri.api.sir.users.interceptor.RequestContextInterceptor;

@SpringBootApplication
@ComponentScan(basePackages = { "com.ricchieri.api" })
public class App implements WebMvcConfigurer {

    @Autowired
    private RequestContext requestContext;

    public static void main(String[] args) {
        run(args);
    }

    public static ConfigurableApplicationContext run(String[] args) {
        return SpringApplication.run(App.class, args);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestContextInterceptor(requestContext));
    }

    @Bean
    @SuppressWarnings("unchecked")
    public InfoEndpoint infoEndpoint() {
        Yaml yaml = new Yaml();
        Map<String, Object> appInfo = (Map<String, Object>) yaml
                .loadAs(this.getClass().getClassLoader().getResourceAsStream("app-info.yml"), Map.class);

        InfoContributor ic = builder -> builder.withDetails(appInfo);
        return new InfoEndpoint(Collections.singletonList(ic));
    }
}
