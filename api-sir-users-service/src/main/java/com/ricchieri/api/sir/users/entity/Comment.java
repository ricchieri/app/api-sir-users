package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Comment extends AuditUser {

    private static final long serialVersionUID = 4601167816625840239L;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Comment audit = (Comment) o;
        return Objects.equals(getMessage(), audit.getMessage())
                && Objects.equals(getCreationUser(), audit.getCreationUser())
                && Objects.equals(getUpdatedUser(), audit.getUpdatedUser())
                && Objects.equals(getCreationDate(), audit.getCreationDate())
                && Objects.equals(getUpdatedDate(), audit.getUpdatedDate()) && Objects.equals(getId(), audit.getId())
                && Objects.equals(isActive(), audit.isActive()) && Objects.equals(getOptlock(), audit.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMessage(), getCreationUser(), getUpdatedDate(), getCreationDate(), getUpdatedDate(),
                getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "Comment [id=" + getId() + ", active=" + isActive() + ", optlock=" + getOptlock() + "creationDate="
                + getCreationDate() + ", updatedDate=" + getUpdatedDate() + "creationUser=" + getCreationUser()
                + "updatedUser=" + getUpdatedUser() + ", message=" + getMessage() + "]";
    }
}