package com.ricchieri.api.sir.users.configuration;

import java.util.concurrent.TimeUnit;

public class CacheConfiguration {

    private Integer duration;
    private Integer maxSize;
    private TimeUnit timeUnit;

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

}
