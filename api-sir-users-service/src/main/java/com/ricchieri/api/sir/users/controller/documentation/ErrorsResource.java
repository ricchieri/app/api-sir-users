package com.ricchieri.api.sir.users.controller.documentation;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

public interface ErrorsResource {

    @ApiOperation(value = "Endpoint which retrieve all the errors of the API")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "the list of the errors", response = List.class) })
    @GetMapping
    @ResponseBody
    ResponseEntity<List<String>> getAllErrors();
}
