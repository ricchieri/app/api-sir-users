package com.ricchieri.api.sir.users.specification.builder;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import com.ricchieri.api.sir.users.request.SearchCriteria;
import com.ricchieri.api.sir.users.enums.SearchOperator;
import com.ricchieri.api.sir.users.specification.BaseSpecification;

public class BaseSpecificationsBuilder {
    private final List<SearchCriteria> params;

    public BaseSpecificationsBuilder(final List<SearchCriteria> params) {
        this.params = params;
    }

    public Optional<BaseSpecification> build() {
        Optional<BaseSpecification> specification = Optional.empty();
        if (params != null && !params.isEmpty()) {

            List<BaseSpecification> specs = params.stream().map(BaseSpecification::new).collect(Collectors.toList());

            BaseSpecification result = specs.get(0);

            for (int i = 1; i < params.size(); i++) {
                result = params.get(i).getOperator().equals(SearchOperator.OR)
                        ? (BaseSpecification) Specification.where(result).or(specs.get(i))
                        : (BaseSpecification) Specification.where(result).and(specs.get(i));
            }
            specification = Optional.of(result);
        }
        return specification;
    }
}
