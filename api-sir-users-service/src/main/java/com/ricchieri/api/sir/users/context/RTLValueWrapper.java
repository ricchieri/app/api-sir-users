package com.ricchieri.api.sir.users.context;

public class RTLValueWrapper {

    private boolean inheritedAsCopy;
    private Object value;

    public RTLValueWrapper(boolean inheritedAsCopy, Object value) {
        this.inheritedAsCopy = inheritedAsCopy;
        this.value = value;
    }

    public boolean isInheritedAsCopy() {
        return inheritedAsCopy;
    }

    public void setInheritedAsCopy(boolean inheritedAsCopy) {
        this.inheritedAsCopy = inheritedAsCopy;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
