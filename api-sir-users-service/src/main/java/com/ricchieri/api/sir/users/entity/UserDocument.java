package com.ricchieri.api.sir.users.entity;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "user_document")
public class UserDocument extends Document {

    private static final long serialVersionUID = 4074547810692808739L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDocument entity = (UserDocument) o;
        return Objects.equals(getUser(), entity.getUser())
                && Objects.equals(getDocumentNumber(), entity.getDocumentNumber())
                && Objects.equals(getDocumentType(), entity.getDocumentType())
                && Objects.equals(getCreationUser(), entity.getCreationUser())
                && Objects.equals(getUpdatedUser(), entity.getUpdatedUser())
                && Objects.equals(getCreationDate(), entity.getCreationDate())
                && Objects.equals(getUpdatedDate(), entity.getUpdatedDate()) && Objects.equals(getId(), entity.getId())
                && Objects.equals(isActive(), entity.isActive()) && Objects.equals(getOptlock(), entity.getOptlock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), getDocumentNumber(), getDocumentType(), getCreationUser(), getUpdatedUser(),
                getCreationDate(), getUpdatedDate(), getId(), isActive(), getOptlock());
    }

    @Override
    public String toString() {
        return "UserDocument [id=" + getId() + ", active=" + isActive() + ", optlock=" + getOptlock() + "creationDate="
                + getCreationDate() + ", updatedDate=" + getUpdatedDate() + "creationUser=" + getCreationUser()
                + "updatedUser=" + getUpdatedUser() + ", documentNumber=" + getDocumentNumber() + "documentType="
                + getDocumentType() + ", user=" + getUser() + "]";
    }
}
