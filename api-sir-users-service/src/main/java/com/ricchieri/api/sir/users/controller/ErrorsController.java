package com.ricchieri.api.sir.users.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.ricchieri.api.sir.users.controller.documentation.ErrorsResource;
import com.ricchieri.api.sir.users.enums.ApiError;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import io.swagger.annotations.Api;

@Api(tags = { "Errors" })
@RestController
@RequestMapping(value = "errors")
public class ErrorsController implements ErrorsResource {

    @Override
    public ResponseEntity<List<String>> getAllErrors() {

        List<String> errors = Lists.newArrayList();

        for (ApiError badRequest : ApiError.values()) {
            errors.add(badRequest.toString());
        }

        return new ResponseEntity<>(errors, HttpStatus.OK);
    }

}
