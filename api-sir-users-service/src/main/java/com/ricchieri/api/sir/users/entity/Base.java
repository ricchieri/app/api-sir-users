package com.ricchieri.api.sir.users.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.util.Objects;

@MappedSuperclass
public abstract class Base implements Serializable {

    private static final long serialVersionUID = -1394398632426929763L;

    @Id
    @Column(name = "id", insertable = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Version
    @Column(name = "optlock", nullable = false)
    private Integer optlock = Integer.valueOf(1);

    @Column(name = "active", nullable = false)
    private Boolean active = Boolean.TRUE;

    public Base() {
    }

    public Base(Long id, Integer optlock, Boolean active) {
        this.id = id;
        this.optlock = optlock;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public int getOptlock() {
        return optlock;
    }

    public void setOptlock(int optlock) {
        this.optlock = optlock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Base base = (Base) o;
        return Objects.equals(id, base.id) && Objects.equals(active, base.active)
                && Objects.equals(optlock, base.optlock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, optlock, active);
    }

    @Override
    public String toString() {
        return "Base [id=" + id + ", optlock=" + optlock + ", active=" + active + "]";
    }
}